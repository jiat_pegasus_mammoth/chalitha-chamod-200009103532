<%@ page import="java.sql.Connection" %>
<%@ page import="com.jiat.web.db.DBConnection" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%--
  Created by IntelliJ IDEA.
  User: chali
  Date: 4/2/2023
  Time: 10:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Delet Data</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">

</head>
<body style="background-color: #b3d3d3">

<div class="container-fluid ">
    <div class="row">

        <h2 class="text-center mt-5 mb-5">Delete User Details</h2>

        <form action="DeleteData" method="POST">

            <%
                String usId = request.getParameter("us_id");
                Connection connection = null;
                try {
                    connection = DBConnection.getConnection();
                    System.out.println(connection);

                    ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM `user` WHERE `id`='" + usId + "'");
                    if (rs.next()) {
            %>

            <div class="col-8 offset-2 border border-1 border-black mt-2 mb-3 bg-white p-3">

                <div>

                    <input type="hidden" value="<%=usId%>" name="user_id"/>
                    <label>Name</label>
                    <input class="form-control" type="text" value="<%=rs.getString("name")%>" name="name"/>
                </div>

                <div>
                    <label>Mobile</label>
                    <input class="form-control" type="text" value="<%=rs.getString("mobile")%>" name="mobile"/>
                </div>
                <%
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    } finally {
                        if (connection != null) {
                            try {
                                connection.close();
                            } catch (SQLException e) {

                            }
                        }
                    }
                %>
                <button class="btn btn-danger mt-2 mb-3 col-3" type="submit">Delete</button>
            </div>
        </form>

    </div>
</div>

</body>
</html>