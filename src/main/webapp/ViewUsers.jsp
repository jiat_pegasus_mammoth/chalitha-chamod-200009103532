<%@ page import="java.sql.Connection" %>
<%@ page import="com.jiat.web.db.DBConnection" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.ResultSet" %><%--
  Created by IntelliJ IDEA.
  User: Deshan Dinuka
  Date: 4/1/2023
  Time: 3:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View Users</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
</head>
<body style="background-color: #b3d3d3">

<div class="container-fluid" >
    <div class="row">

        <div class="col-12">
            <h1 class="text-center mt-5 mb-5">User Details</h1>
        </div>

        <div class="col-8 offset-2 border border-1 border-black mt-2 mb-2 bg-white p-3">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Mobile</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>

                <%
                    Connection connection = null;
                    try {
                        connection = DBConnection.getConnection();
                        System.out.println(connection);

                        ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM `user`");
                        while (rs.next()) {
                %>
                <tr>
                    <td><%=rs.getString("id")%>
                    </td>
                    <td><%=rs.getString("name")%>
                    </td>
                    <td><%=rs.getString("mobile")%>
                    </td>
                    <td>
                        <a href="UpdateUsers.jsp?us_id=<%=rs.getString("id")%>" class="btn bg-secondary fs-6">Update</a>
                        <a  href="DeleteData.jsp?us_id=<%=rs.getString("id")%>" class="btn bg-danger fs-6">Delete</a>
                    </td>
                </tr>
                <%
                        }


                    } catch (Exception ex) {
                        ex.printStackTrace();
                    } finally {
                        if (connection != null) {
                            try {
                                connection.close();
                            } catch (SQLException e) {

                            }
                        }
                    }

                %>
                </tbody>
            </table>
        </div>

    </div>
</div>

</body>
</html>
