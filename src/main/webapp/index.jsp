<%--
  Created by IntelliJ IDEA.
  User: Deshan Dinuka
  Date: 3/30/2023
  Time: 11:42 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register New Users</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
</head>
<body style="background-color: #b3d3d3">

<div class="container-fluid" >
    <div class="row">

        <div class="col-12 mt-5 mb-5">
            <h1 class="text-center">User Details</h1>
        </div>

        <div class="col-8 offset-2 border border-1 border-black mt-2 mb-2 bg-white p-3">
            <div class="row">

                <form action="AddData" method="POST">
                    <div class="col-12">
                        <label>Enter Name</label>
                        <input type="text" class="form-control " name="name"/>
                    </div>

                    <div class="col-12 mt-2">
                        <label>Enter Mobile Number</label>
                        <input type="text" class="form-control " name="mobile"/>
                    </div>

                    <div class="row">

                    <div class="mt-2 text-center">
                        <button class="btn btn-primary shadow-none text-white col-5" type="submit">Add New User</button>

                        <a href="ViewUsers.jsp" class="btn text-white bg-secondary col-5">View Users</a>
                    </div>

                    </div>

                </form>
            </div>
        </div>


    </div>
</div>

</body>
</html>