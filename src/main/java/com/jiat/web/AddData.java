package com.jiat.web;

import com.jiat.web.db.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "AddData", urlPatterns = "/AddData")
public class AddData extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("name");
        String mobile = request.getParameter("mobile");

        Connection connection = null;

        try {
            connection = DBConnection.getConnection();
            connection.createStatement().executeUpdate("INSERT INTO `user`(`name`,`mobile`) VALUES ('" + name + "','" + mobile + "')");
            System.out.println("Data insert successful..........");
            response.getWriter().print("<script>alert('User Details Added Successful!'); window.location='ViewUsers.jsp';</script>");

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {

                }
            }
        }

    }
}
