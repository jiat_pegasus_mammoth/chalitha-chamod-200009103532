package com.jiat.web;


import com.jiat.web.db.DBConnection;

        import javax.servlet.ServletException;
        import javax.servlet.annotation.WebServlet;
        import javax.servlet.http.HttpServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;
        import java.io.IOException;
        import java.sql.Connection;
        import java.sql.SQLException;

@WebServlet(name = "DeleteData", urlPatterns = "/DeleteData")
public class DeleteData extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String userId = request.getParameter("user_id");


        Connection connection = null;

        try {
            connection = DBConnection.getConnection();
            connection.createStatement().executeUpdate("DELETE FROM  `user`  WHERE `id`='"+userId+"'");
            response.getWriter().print("<script>alert('Delete Successful!'); window.location='ViewUsers.jsp';</script>");

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {

                }
            }
        }

    }
}
